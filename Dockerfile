FROM php:5.6-fpm

RUN apt-get update && apt-get install -y  \
    php-pear php5-dev \
    libghc-postgresql-libpq-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/include/postgresql/ \
    && docker-php-ext-install pgsql pdo_pgsql \
    && pecl install dbase \
    && docker-php-ext-enable dbase

ADD ./convert.php /