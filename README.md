# Export KLADR to postgres

### Preparation

Extract content of http://gnivc.ru/html/gnivcsoft/KLADR/Base.7z to ./data directory.
Edit /conf/settings.php

### Run it:

1. `$ cd ./kladr`
2. `$ docker build -t kladr .`
3. `$ docker run -ti -v $(pwd)/data:/data -v $(pwd)/conf:/conf kladr php /convert.php`


### Quering db

#Find geo objects with parents

WITH RECURSIVE parents AS (
select *, true as prime from geo where "type" = 'street' AND title like '%Строителей%'

UNION

SELECT geo., false as prime
FROM geo, parents
WHERE geo.geo_id = parents.parent_id
) SELECT FROM parents;



#Find geo objects with children

WITH RECURSIVE tree AS (
select geo_id, ARRAY[]::bigint[] AS parents from geo where title = 'Томск'

UNION ALL

SELECT geo.geo_id, tree.parents || geo.parent_id
FROM geo, tree
WHERE geo.parent_id = tree.geo_id
) SELECT * FROM tree;