<?php
$settings = require '/conf/settings.php';

$sql = <<<'EOT'
DROP TABLE IF EXISTS geo;

DROP TYPE IF EXISTS t_geo_type;
CREATE TYPE t_geo_type AS ENUM('country', 'region', 'city', 'street');

CREATE TABLE geo
(
  geo_id bigint NOT NULL,
  parent_id bigint,
  type t_geo_type,
  title character varying(100),
  suffix character varying(10),
  prefix character varying(10),
  CONSTRAINT geo_pkey PRIMARY KEY (geo_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE geo
  OWNER TO "%s";

CREATE INDEX parent_key ON geo (parent_id);
CREATE INDEX type_title_key ON geo (type, title);
EOT;

$sql = sprintf($sql, $settings['db']['username']);

try {
    $db = new PDO($settings['db']['dsn'], $settings['db']['username'], $settings['db']['password']);
} catch (PDOException  $e) {
    echo "Error: " . $e;
}

$db->exec($sql);

$dbf = dbase_open('/data/KLADR.DBF', 0);
if ($dbf) {
    $record_numbers = dbase_numrecords($dbf);

    $sth = $db->prepare('INSERT INTO geo ("geo_id", "parent_id",  "type", "title", "suffix") VALUES (?,?,?,?,?)');

    for ($i = 1; $i <= $record_numbers; $i++) {
        $row = dbase_get_record_with_names($dbf, $i);
        $utf_row = array_map(function ($field) {
            return iconv("CP866", "UTF-8", $field);
        }, $row);

        $kladr = kladr($utf_row['CODE']);

        if ($kladr['code']) {
            continue;
        }

        $data = [
            $kladr['num'],
            $kladr['parent'],
            $kladr['type'],
            trim($utf_row['NAME']),
            trim($utf_row['SOCR']),
        ];

        if (!$sth->execute($data)) {
            print_r($data);
        };

    }

}
dbase_close($dbf);


$dbf = dbase_open('/data/STREET.DBF', 0);
if ($dbf) {
    $record_numbers = dbase_numrecords($dbf);

    $sth = $db->prepare('INSERT INTO geo ("geo_id", "parent_id",  "type", "title", "suffix") VALUES (?,?,?,?,?)');

    for ($i = 1; $i <= $record_numbers; $i++) {
        $row = dbase_get_record_with_names($dbf, $i);
        $utf_row = array_map(function ($field) {
            return iconv("CP866", "UTF-8", $field);
        }, $row);

        $kladr = street($utf_row['CODE']);

        if ($kladr['code']) {
            continue;
        }

        $data = [
            $kladr['num'],
            $kladr['parent'],
            $kladr['type'],
            trim($utf_row['NAME']),
            trim($utf_row['SOCR']),
        ];

        if (!$sth->execute($data)) {
            print_r($data);
        };

    }

}
dbase_close($dbf);

function kladr($kladr)
{
    $region = (int)substr($kladr, 0, 2);
    $raion = (int)substr($kladr, 2, 3);
    $gorod = (int)substr($kladr, 5, 3);
    $np = (int)substr($kladr, 8, 3);
    $code = (int)substr($kladr, 11, 2);

    if ($np) {
        $num = (int)substr($kladr, 0, 11);
        if ($gorod) {
            $parent = (int)substr($kladr, 0, 8);
        } elseif ($raion) {
            $parent = (int)substr($kladr, 0, 5);
        } else {
            $parent = (int)substr($kladr, 0, 2);
        }
    } else if ($gorod) {
        $num = (int)substr($kladr, 0, 8);
        if ($raion) {
            $parent = (int)substr($kladr, 0, 5);
        } else {
            $parent = (int)substr($kladr, 0, 2);
        }
    } else if ($raion) {
        $num = (int)substr($kladr, 0, 5);
        $parent = (int)substr($kladr, 0, 2);
    } else {
        $num = (int)substr($kladr, 0, 2);
        $parent = null;
    }

    //'country', 'region', 'city', 'street'
    if ($region || $raion) {
        $type = 'region';
    }

    if ($gorod || $np) {
        $type = 'city';
    }

    return compact('type', 'region', 'raion', 'gorod', 'np', 'code', 'num', 'parent');
}


function street($kladr)
{
    //17
    $region = (int)substr($kladr, 0, 2);
    $raion = (int)substr($kladr, 2, 3);
    $gorod = (int)substr($kladr, 5, 3);
    $np = (int)substr($kladr, 8, 3);
    $street = (int)substr($kladr, 11, 4);
    $code = (int)substr($kladr, 15, 2);

    $num = (int)substr($kladr, 0, 15);

    if ($np) {
        $parent = (int)substr($kladr, 0, 11);
    } else if ($gorod) {
        $parent = (int)substr($kladr, 0, 8);
    } else {
        // Москва/Питер
        $parent = (int)substr($kladr, 0, 2);
    }

    $type = 'street';

    return compact('type', 'region', 'raion', 'gorod', 'np', 'street','code', 'num', 'parent');
}