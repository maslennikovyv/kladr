<?php
return [
    // db settings
    'db' => [
        'dsn' => 'pgsql:host=localhost;dbname=geo',
        'username' => 'postgres',
        'password' => 'secret',
    ],
];
